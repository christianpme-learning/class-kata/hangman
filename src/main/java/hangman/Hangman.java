package hangman;

import java.util.ArrayList;
import java.util.List;

public class Hangman {
	static final String METHOD_NAME = "tryLetter";

	private String searchedWord = "";
	private final StringBuilder resultWordBuilder;

	public Hangman(final String searchedWord) {
		this.searchedWord = searchedWord;
		this.resultWordBuilder = new StringBuilder();
		for (int i = 0; i < this.searchedWord.length(); ++i) {
			this.resultWordBuilder.append('-');
		}
	}

	public String tryLetter(final Character character) {
		int offset = 0;
		for (final char c : this.searchedWord.toCharArray()) {
			if (character.equals(c) || Character.toUpperCase(character) == c) {
				this.resultWordBuilder.replace(offset, offset + 1, Character.toString(c));
			}
			++offset;
		}
		return this.resultWordBuilder.toString();
	}

	public List<String> buildTableHeader() {
		final ArrayList<String> table = new ArrayList<>();

		int actualRowLength = Hangman.METHOD_NAME.length() + 1;

		// header
		final StringBuilder headerBuilder = new StringBuilder();
		this.columnBuilder(headerBuilder, actualRowLength, "Method");

		actualRowLength += this.searchedWord.length() + 3;
		this.columnBuilder(headerBuilder, actualRowLength, "Input");

		actualRowLength += this.searchedWord.length();
		this.columnBuilder(headerBuilder, actualRowLength, "Output");
		table.add(headerBuilder.toString());

		// header-line
		final StringBuilder lineBuilder = new StringBuilder();
		for (int i = 0; i < actualRowLength; ++i) {
			lineBuilder.append("-");
		}
		table.add(lineBuilder.toString());

		// input row
		final StringBuilder inputRowBuilder = new StringBuilder();
		this.columnBuilder(inputRowBuilder, Hangman.METHOD_NAME.length() + 1, "ctor");

		this.columnBuilder(inputRowBuilder, actualRowLength, this.searchedWord);
		table.add(inputRowBuilder.toString());

		return table;
	}

	private StringBuilder columnBuilder(final StringBuilder builder, final int lineLength, final String string) {
		builder.append(string);
		final int whitespaces = lineLength - builder.length();
		for (int i = 0; i < whitespaces; ++i) {
			builder.append(" ");
		}
		return builder;
	}

	public String buildTableRow(final Character character) {
		final StringBuilder rowBuilder = new StringBuilder();

		// column 1
		rowBuilder.append(Hangman.METHOD_NAME + " ");

		// column 2
		rowBuilder.append(character);
		final int whitespaces = this.searchedWord.length() + 2;
		for(int i=0; i<whitespaces; ++i){
			rowBuilder.append(" ");
		}

		//column 3
		rowBuilder.append(this.tryLetter(character));

		return rowBuilder.toString();
	}
}