package hangman;

public class App 
{
    public static void main( String[] args )
    {
        Hangman hangman = new Hangman("Developer");
        
        for(String row : hangman.buildTableHeader()){
            System.out.println(row);
        }

        char[] inputs = {'u','e','n','o','r','a','d','l','p','v'};

        for(char c : inputs){
            System.out.println(hangman.buildTableRow(c));
        }
    }
}
