package hangman;

import static org.junit.Assert.assertTrue;

import java.util.ArrayList;
import java.util.List;

import org.junit.Test;

public class HangmanTest 
{
    @Test
    public void hangmanInitTest(){
        Hangman hangman = new Hangman("Developer");
        String actual = hangman.tryLetter(' ');
        String expected = "---------";
        assertTrue(expected.equals(actual));
    }

    @Test
    public void tryLetterTest(){
        Hangman hangman = new Hangman("Developer");
        String actual = hangman.tryLetter('e');
        String expected = "-e-e---e-";
        assertTrue(expected.equals(actual));
    }

    @Test
    public void tryLetterCaseSensitiveTest(){
        Hangman hangman = new Hangman("Developer");
        hangman.tryLetter('e');
        String actual = hangman.tryLetter('d');
        String expected = "De-e---e-";
        assertTrue(expected.equals(actual));
    }

    @Test
    public void tryWordDeveloperTest(){
        Hangman hangman = new Hangman("Developer");
        hangman.tryLetter('e');
        hangman.tryLetter('o');
        hangman.tryLetter('r');
        hangman.tryLetter('D');
        hangman.tryLetter('l');
        hangman.tryLetter('v');
        String actual = hangman.tryLetter('p');

        String expected = "Developer";
        assertTrue(actual.equals(expected));
    }

    @Test
    public void buildTableTest(){
        Hangman hangman = new Hangman("Developer");
        List<String> actual = hangman.buildTableHeader();

        List<String> expected = new ArrayList<>();
        expected.add("Method    Input       Output   ");
        expected.add("-------------------------------");
        expected.add("ctor      Developer            ");

        assertTrue(expected.equals(actual));
    }

    @Test
    public void buildTableInputTest(){
        Hangman hangman = new Hangman("Developer");
        String actual = hangman.buildTableRow('e');

        String expected = "tryLetter e           -e-e---e-";

        assertTrue(expected.equals(actual));
    }
}